from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


@login_required
def receipt_list(request):
    vendor_receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": vendor_receipt,
    }
    return render(request, "receipts/list.html", context)


@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            form.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


@login_required
def category_list(request):
    category_list = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category_list": category_list,
    }
    return render(request, "receipts/things_category.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        create_category_form = ExpenseCategoryForm(request.POST)
        if create_category_form.is_valid():
            new_category = create_category_form.save(False)
            new_category.owner = request.user
            create_category_form.save()
            return redirect("category_list")
    else:
        create_category_form = ExpenseCategoryForm()

    context = {
        "create_category": create_category_form,
    }
    return render(request, "receipts/create_category.html", context)


@login_required
def account_list(request):
    account_list = Account.objects.filter(owner=request.user)
    context = {
        "account_list": account_list,
    }
    return render(request, "receipts/account_things.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        create_account_form = AccountForm(request.POST)
        if create_account_form.is_valid():
            new_account = create_account_form.save(False)
            new_account.owner = request.user
            create_account_form.save()
            return redirect("account_list")
    else:
        create_account_form = AccountForm()

    context = {
        "create_account": create_account_form,
    }
    return render(request, "receipts/create_account.html", context)
